import useSWR from 'swr';
import { IGame, PageResponse } from '../general/types/Game';
import query from '../graphql/query';
import fetcher from './fetcher';

export interface GetGamePaginatedInput {
  limit: number;
  page: number;
}

export const useGetAllGames = (input: GetGamePaginatedInput) => {
  const { data, error, mutate } = useSWR<{ getAllGames: PageResponse<IGame> }>(
    [query.getAllGames, input],
    fetcher,
    {
      revalidateOnFocus: false,
      revalidateOnReconnect: false,
    },
  );

  return {
    data,
    isLoading: !error && !data,
    error,
    mutate,
  };
};

export const useGetGameByName = (vars: { name: string }) => {
  const { data, error, mutate } = useSWR<{ getGame: IGame }>([query.getGame, vars], fetcher);

  return {
    data,
    isLoading: !error && !data,
    error: error?.response?.errors[0]?.message,
    mutate,
  };
};

export const useGetGameCount = () => {
  const { data, error, mutate } = useSWR<{ getGameCount: number }>(query.getGameCount, fetcher);

  return {
    data,
    isLoading: !error && !data,
    error: error?.response?.errors[0]?.message,
    mutate,
  };
};
