import useSWR from 'swr';
import { IPlayer, PlayerHistory } from '../general/types/Player';
import query from '../graphql/query';
import fetcher from './fetcher';

export const useGetPlayers = () => {
  const { data, error, mutate } = useSWR<{ getAllPlayers: IPlayer[] }>(
    query.getAllPlayers,
    fetcher,
  );

  return {
    data,
    isLoading: !error && !data,
    error,
    mutate,
  };
};

export const useGetPlayersFull = (sortBy: string) => {
  const { data, error, mutate } = useSWR<{ getAllPlayers: IPlayer[] }>(
    [query.getAllPlayersFull, { sortBy }],
    fetcher,
  );

  return {
    data,
    isLoading: !error && !data,
    error: error?.response?.errors[0]?.message,
    mutate,
  };
};

export const useGetPlayerByHandle = (handle: string) => {
  const { data, error, mutate } = useSWR<{ getPlayerByHandle: IPlayer }>(
    [query.getPlayerByHandle, { handle }],
    fetcher,
  );

  return {
    data,
    isLoading: !error && !data,
    error: error?.response?.errors[0]?.message,
    mutate,
  };
};

export const useGetPlayerHistory = (handle: string) => {
  const { data, error, mutate } = useSWR<{ getPlayerHistory: PlayerHistory[] }>(
    [query.getPlayerHistory, { handle }],
    fetcher,
  );

  return {
    data,
    isLoading: !error && !data,
    error: error?.response?.errors[0]?.message,
    mutate,
  };
};

export const useGetPlayersHistory = (handles: any) => {
  const { data, error, mutate } = useSWR<{ getPlayersHistory: PlayerHistory[] }>(
    [query.getPlayersHistory, { handles }],
    fetcher,
  );

  return {
    data,
    isLoading: !error && !data,
    error: error?.response?.errors[0]?.message,
    mutate,
  };
};

export const useGetStatByYear = (handle: any) => {
  const { data, error, mutate } = useSWR<{ getStatByYear: IPlayer[] }>(
    [query.getStatByYear, { handle }],
    fetcher,
  );

  return {
    data,
    isLoading: !error && !data,
    error: error?.response?.errors[0]?.message,
    mutate,
  };
};
