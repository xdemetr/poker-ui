import { GraphQLClient } from 'graphql-request';

export const apiUrl = process.env.REACT_APP_API_URL || '';

const localData = JSON.parse(localStorage.getItem('login') as any | null);

export const graphQLClient = new GraphQLClient(apiUrl, {
  headers: {
    authorization: localData ? localData?.token : null,
  },
});

const fetcher = (query: string, vars: any) => graphQLClient.request(query, vars);

export default fetcher;
