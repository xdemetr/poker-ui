import useSWR from 'swr';
import query from '../graphql/query';
import fetcher, { graphQLClient } from './fetcher';

export const useProfile = () => {
  const { data, mutate, error } = useSWR(query.user, fetcher, {
    revalidateOnFocus: false,
    revalidateOnReconnect: true,
    shouldRetryOnError: false,
  });

  const logout = async () => {
    localStorage.removeItem('login');
    graphQLClient.setHeader('authorization', '');
    await mutate(null);
  };

  return {
    data,
    mutate,
    isLoading: !error && !data,
    logout,
    isExp: data ? data.user.exp < Date.now() / 1000 : false,
  };
};
