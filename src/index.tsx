import React from 'react';
import { createRoot } from 'react-dom/client';
import { RouterProvider } from 'react-router-dom';
import './assets/styles/app.scss';
import { router } from './components/Root/router';
// import './assets/zephyr.css';
// import './assets/app.css';


const container = document.getElementById('root');

if (!container) {
  throw new Error('not root container');
}

const root = createRoot(container);

root.render(
  <RouterProvider router={router} />
);
