import { gql } from 'graphql-request';
import { BASE_GAME_INFO, BASE_PLAYER_INFO, FULL_GAME_INFO, FULL_PLAYER_INFO } from './graphqlTypes';

const AppQuery = {
  user: gql`
    query user {
      user {
        email
        isAdmin
        iat
        exp
      }
    }
  `,

  refresh: gql`
    query refresh {
      refresh {
        token
      }
    }
  `,

  login: gql`
    query login($email: String!, $password: String!) {
      login(input: { email: $email, password: $password }) {
        token
      }
    }
  `,

  updateUser: gql`
    mutation ($profileInput: ProfileInput!) {
      updateUser(input: $profileInput) {
        id
        email
      }
    }
  `,

  getAllPlayers: gql`
    ${BASE_PLAYER_INFO}
    query getAllPlayers {
      getAllPlayers(input: {}) {
        ...basePlayerInfo
      }
    }
  `,

  getStatByYear: gql`
    query getStatByYear($handle: Int!) {
      getStatByYear(input: { handle: $handle }) {
        name
        results {
          game {
            name
            date
          }
          result
        }
      }
    }
  `,

  getAllPlayersFull: gql`
    query getAllPlayers($sortBy: SortBy!) {
      getAllPlayers(input: { sortBy: $sortBy }) {
        id
        name
        handle
        balance
        gameCount
        isShowInRating
        maxSeriesOfWin
        maxSeriesOfLoose
      }
    }
  `,

  getPlayerByHandle: gql`
    ${FULL_PLAYER_INFO}
    query getPlayerByHandle($handle: String!) {
      getPlayerByHandle(input: { handle: $handle }) {
        ...fullPlayerInfo
      }
    }
  `,

  getPlayerHistory: gql`
    query getPlayerHistory($handle: String!) {
      getPlayerHistory(input: { handle: $handle }) {
        date
        value
        name
      }
    }
  `,

  getPlayersHistory: gql`
    query getPlayersHistory($handles: [ID!]!) {
      getPlayersHistory(input: { handle: $handles }) {
        date
        value
        name
      }
    }
  `,

  getGameCount: gql`
    query {
      getGameCount
    }
  `,

  getAllGames: gql`
    query getAllGames($limit: Int, $page: Int) {
      getAllGames(input: { limit: $limit, page: $page }) {
        pageCount
        itemCount
        has_more
        data {
          name
        }
        pages {
          number
        }
      }
    }
  `,

  getGame: gql`
    ${FULL_GAME_INFO}
    query getGame($name: String!) {
      getGame(input: { name: $name }) {
        ...fullGameInfo
      }
    }
  `,

  createGame: gql`
    ${BASE_GAME_INFO}
    mutation ($gameInput: GameInput!) {
      createGame(input: $gameInput) {
        ...baseGameInfo
      }
    }
  `,
  saveGameResult: gql`
    ${BASE_GAME_INFO}
    mutation saveGameResult($resultInput: SaveGameInput!) {
      saveGameResult(input: $resultInput) {
        ...baseGameInfo
        results
      }
    }
  `,
  createPlayer: gql`
    ${BASE_PLAYER_INFO}
    mutation ($playerInput: PlayerInput!) {
      createPlayer(input: $playerInput) {
        ...basePlayerInfo
        isRegular
      }
    }
  `,
  deleteGame: gql`
    mutation deleteGame($id: ID!) {
      deleteGame(input: { id: $id })
    }
  `,
};

export default AppQuery;
