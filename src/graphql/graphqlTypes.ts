import { gql } from 'graphql-request';

export const BASE_PLAYER_INFO = gql`
  fragment basePlayerInfo on Player {
    id
    name
    handle
  }
`;

export const FULL_PLAYER_INFO = gql`
  ${BASE_PLAYER_INFO}
  fragment fullPlayerInfo on Player {
    ...basePlayerInfo
    gameCount
    isRegular
    isShowInRating
    maxSeriesOfLoose
    maxSeriesOfWin
    balance
    attendance
    results {
      game {
        id
        name
        date
      }
      result
    }
  }
`;

export const BASE_GAME_INFO = gql`
  fragment baseGameInfo on Game {
    id
    name
    date
  }
`;

export const FULL_GAME_INFO = gql`
  ${BASE_GAME_INFO}
  ${BASE_PLAYER_INFO}

  fragment fullGameInfo on Game {
    ...baseGameInfo
    results
    isBigGame
    buyIn
    players {
      ...basePlayerInfo
    }
  }
`;
