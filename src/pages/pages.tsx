import AuthPage from './Profile/AuthPage';
import ProfilePage from './Profile/ProfilePage';
// import RegistrationPage from './RegistrationPage/RegistrationPage';
import GamePage from './GamePage/GamePage';
import PlayersPage from './PlayersPage/PlayersPage';
import EditPlayerPage from './PlayersPage/EditPlayerPage';
import NewGamePage from './GamePage/NewGamePage';
import ChartPage from './ChartPage/ChartPage';
import EditGamePage from './GamePage/EditGamePage';
import HomePage from './HomePage/HomePage';
import ChallengePage from './ChallengePage/ChallengePage';

export {
  AuthPage,
  ProfilePage,
  PlayersPage,
  GamePage,
  ChartPage,
  EditPlayerPage,
  EditGamePage,
  NewGamePage,
  HomePage,
  ChallengePage,
};
