import React from 'react';
import PageContainer from '../../components/PageContainer/PageContainer';
import Img from '../../assets/images/main.jpg';
import './HomePage.scss';

const HomePage = () => {
  return (
    <PageContainer
      mod="home-page"
      body={
        <div className="row">
          <div className="row__image home-page__image-box">
            <img src={Img} alt="home" className="home-page__image" />
          </div>
          <div className="row__text">
            <p>Все игроки являются вымышленными. Никто тут в покер не играет.</p>
            <p>
              &mdash; Фронт сделан на React. Находится на{' '}
              <a href="https://gitlab.com/xdemetr/poker-ui">Gitlab</a>.
            </p>
            <p>
              &mdash; Бэк работает на NodeJS, MongoDB, graphQL находится на{' '}
              <a href="https://github.com/xdemetr/poker-server-graph">Github</a>.
            </p>
          </div>
        </div>
      }
    />
  );
};

export default HomePage;
