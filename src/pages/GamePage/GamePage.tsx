import React, { useState } from 'react';
import { Navigate, NavLink, useParams } from 'react-router-dom';
import { useGetAllGames } from '../../api/games';
import ContentList from '../../components/ContentList/ContentList';
import PageContainer from '../../components/PageContainer/PageContainer';
import Paginator from '../../components/Paginator/Paginator';
import ProtectedElement from '../../components/ProtectedElement/ProtectedElement';
import CONSTANTS from '../../general/constants';
import GameDetails from './components/GameDetails/GameDetails';
import './GamePage.scss';

const GamePage = () => {
  const { name } = useParams<{ name: string }>();
  const [page, setPage] = useState(1);

  const { data: games, isLoading } = useGetAllGames({ page, limit: 10 });

  if (!name && games?.getAllGames) {
    return <Navigate to={`${CONSTANTS.ROUTE.GAMES}/${games.getAllGames.data[0].name}`} />;
  }

  const setPageHandle = async (page: number) => {
    setPage(page);
  };

  return (
    <PageContainer
      mod="game-page"
      aside={
        <>
          <ProtectedElement classNames="action-button">
            <NavLink to={`${CONSTANTS.ROUTE.GAME_NEW}`} className="btn btn-outline-primary btn-lg">
              {CONSTANTS.BUTTON.NEW_GAME}
            </NavLink>
          </ProtectedElement>

          <ContentList content={games?.getAllGames?.data || null} type={'games'} loading={isLoading}
                       title="Список игр" />
          <Paginator currentPage={page} onPageChanged={setPageHandle} pages={games?.getAllGames?.pages || null} />
        </>
      }
      body={<GameDetails />}
    />
  );
};

export default React.memo(GamePage);
