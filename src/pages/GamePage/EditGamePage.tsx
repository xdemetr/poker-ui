import { FormikValues } from 'formik/dist/types';
import React, { useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { graphQLClient } from '../../api/fetcher';
import { useGetGameByName } from '../../api/games';
import Alert from '../../components/Alert/Alert';
import PageContainer from '../../components/PageContainer/PageContainer';
import Spinner from '../../components/Spinner/Spinner';
import CONSTANTS from '../../general/constants';
import query from '../../graphql/query';
import GameSaveResultForm from './components/GameForm/GameSaveResultForm';

const EditGamePage = () => {
  const { name } = useParams<{ name: string }>();
  const navigate = useNavigate();

  const [error, setError] = useState('');
  const { data: game, isLoading: loading } = useGetGameByName({ name: name! });

  const saveResultGame = async (resultInput: FormikValues) => {
    try {
      await graphQLClient.request(query.saveGameResult, { resultInput });
      navigate(`${CONSTANTS.ROUTE.GAMES}`);

    } catch (e: any) {
      setError(e.response.errors[0].message);
    }
  };

  const removeGame = async (id: string) => {
    await graphQLClient.request(query.deleteGame, { id });
    navigate(`${CONSTANTS.ROUTE.GAMES}`);
  };

  if (!game?.getGame) {
    return null;
  }

  return (
    <PageContainer
      mod="edit-game-page"
      body={
        <section className="edit-game-form card">
          <header className="card__header">
            <h1>{CONSTANTS.PAGES.GAME_SAVE_RESULT}</h1>
          </header>

          <div className="card__body">
            {loading && <Spinner />}
            {error && <Alert type={'danger'} message={error} />}
            <GameSaveResultForm game={game.getGame} handleSave={saveResultGame} handleRemove={removeGame} />
          </div>
        </section>
      }
    />
  );
};
export default EditGamePage;
