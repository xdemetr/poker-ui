import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { graphQLClient } from '../../api/fetcher';
import { useGetPlayers } from '../../api/players';
import Alert from '../../components/Alert/Alert';
import PageContainer from '../../components/PageContainer/PageContainer';
import Spinner from '../../components/Spinner/Spinner';
import CONSTANTS from '../../general/constants';
import query from '../../graphql/query';
import GameForm from './components/GameForm/GameForm';

const NewGamePage = () => {
  const navigate = useNavigate();

  const [error, setError] = useState('');
  const { data: players, isLoading: loading } = useGetPlayers();

  if (!players?.getAllPlayers) {
    return null;
  }

  const handleSave = async (gameInput: any) => {
    try {
      const data = await graphQLClient.request(query.createGame, { gameInput });
      navigate(`${CONSTANTS.ROUTE.GAMES}/${data.createGame.name}`);

    } catch (e: any) {
      setError(e.response.errors[0].message);
    }
  };

  return (
    <PageContainer
      mod="new-game-page new-game"
      body={
        <section className="card">
          <header className="card__header">
            <h1>{CONSTANTS.PAGES.GAME_NEW}</h1>
          </header>
          <div className="card__body">
            {loading && <Spinner />}
            {error && <Alert type={'danger'} message={error} />}
            <GameForm players={players.getAllPlayers} handleSave={handleSave} />
          </div>
        </section>
      }
    />
  );
};

export default NewGamePage;
