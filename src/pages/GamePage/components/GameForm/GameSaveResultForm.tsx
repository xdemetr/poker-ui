import React from 'react';
import { ErrorMessage, Field, FieldArray, Form, Formik } from 'formik';
import * as Yup from 'yup';
import { IGame } from '../../../../general/types/Game';
import CONSTANTS from '../../../../general/constants';
import { IPlayer } from '../../../../general/types/Player';
import FormDatePicker2 from '../../../../components/Forms/FormDatePicker2';
import moment from 'moment';
import CancelButton from '../../../../components/Forms/CancelButton';

interface Props {
  game: IGame;
  handleSave: (e: any) => void;
  handleRemove: (id: string) => void;
}

const GameSaveResultForm: React.FC<Props> = ({ game, handleSave, handleRemove }) => {
  function setEmptyResults(arr: any) {
    return arr.map(() => 0);
  }

  const initialValues = {
    id: game.id,
    name: game.name,
    date: game.date,
    buyIn: game?.buyIn || 500,
    isBigGame: game?.isBigGame || false,
    players: game?.players || [],
    results: game?.results.length > 0 ? game.results : setEmptyResults(game.players),
  };

  const validationSchema = Yup.object().shape({
    date: Yup.string().required('Required'),
    results: Yup.array().of(Yup.number().required('req')),
  });

  const onSubmit = (data: IGame) => {
    const res = {
      ...data,
      id: game.id,
      name: moment(data.date).format('DD-MM-YYYY'),
      results: data.results.map((r) => +r),
      players: data.players.map((r: IPlayer) => r.id),
    };
    handleSave(res);
  };

  return (
    <div className="game-save-result-form form">
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
        {({ errors, values, touched, setFieldValue, setValues }) => (
          <Form>
            <FormDatePicker2 name="date" />

            <table className="table table-bordered game-save-result-form__table">
              <tbody>
              {/*//@ts-ignore*/}
              <FieldArray name="players">
                {() =>
                  game.players.map((player, i: number) => {
                    //@ts-ignore
                    const ticketErrors = (errors.results?.length && errors.results[i]) || {};
                    //@ts-ignore
                    const ticketTouched = (touched.results?.length && touched.results[i]) || {};

                    return (
                      <tr key={player.handle}>
                        <td>
                          <label>{player.name}</label>
                        </td>
                        <td>
                          <div className="form-group">
                            <Field
                              name={`results.${i}`}
                              type="text"
                              className={
                                'form-control' +
                                //@ts-ignore
                                (ticketErrors.name && ticketTouched.name ? ' is-invalid' : '')
                              }
                            />
                            {/*//@ts-ignore*/}
                            <ErrorMessage
                              name={`results${i}`}
                              component="div"
                              className="invalid-feedback"
                            />
                          </div>
                        </td>

                        <td>
                          <div className="btn-group">
                            <button
                              type="button"
                              className="btn btn-sm btn-outline-danger"
                              onClick={() => setFieldValue(`results[${i}]`, -game.buyIn)}>
                              -{game.buyIn}
                            </button>

                            <button
                              type="button"
                              className="btn btn-sm btn-outline-danger"
                              onClick={() => setFieldValue(`results[${i}]`, -game.buyIn * 2)}>
                              -{game.buyIn * 2}
                            </button>
                          </div>
                        </td>
                      </tr>
                    );
                  })
                }
              </FieldArray>
              </tbody>
            </table>

            <div className="form__actions">
              <button type="submit" className="btn btn-primary">
                {CONSTANTS.BUTTON.SAVE}
              </button>

              <CancelButton backTo={`${CONSTANTS.ROUTE.GAMES}/${game.name}`} />

              <button
                type="button"
                className="btn btn-danger ms-auto"
                onClick={() => handleRemove(game.id)}>
                {CONSTANTS.BUTTON.DELETE}
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};
export default GameSaveResultForm;
