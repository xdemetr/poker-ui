import React from 'react';
import { useFormik } from 'formik';
import moment from 'moment';
import FormCheckbox from '../../../../components/Forms/FormCheckbox';
import { IPlayer } from '../../../../general/types/Player';
import CONSTANTS from '../../../../general/constants';
import FormInput from '../../../../components/Forms/FormInput';
import FormDatePicker from '../../../../components/Forms/FormDatePicker';
import CancelButton from '../../../../components/Forms/CancelButton';
import * as Yup from 'yup';

interface Props {
  players: IPlayer[] | null;
  handleSave: (data: any) => void;
}

const GameForm: React.FC<Props> = ({ players, handleSave }) => {
  const validationSchema = Yup.object().shape({
    players: Yup.array().min(2),
    buyIn: Yup.number().required(),
  });

  const formik = useFormik({
    initialValues: {
      name: '',
      buyIn: 500,
      isBigGame: false,
      players: [],
    },
    enableReinitialize: true,
    onSubmit: (values) => {
      const req = {
        ...values,
        buyIn: +values.buyIn,
        name: formik.values.name ? moment(values.name).format('DD-MM-YYYY') : '',
      };
      handleSave(req);
    },
    validationSchema,
    isInitialValid: true,
  });

  if (!players) {
    return null;
  }

  const checkedCount = () => (
    <div className="new-game__header">
      Участники: <span className="badge badge_info">{formik.values.players.length}</span>
    </div>
  );

  const hasError = !!(formik.errors.players || formik.errors.buyIn);

  return (
    <form onSubmit={formik.handleSubmit} className="form">
      {checkedCount()}

      <div className="new-game__info">
        <div className="new-game__info-date">
          <FormDatePicker
            name="name"
            id="name"
            label="Дата"
            handleChange={formik.setFieldValue}
            touched={formik.touched}
            handleBlur={formik.handleBlur}
            errors={formik.errors}
            values={formik.values}
          />
        </div>

        <div className="new-game__info-buyin">
          <FormInput
            id="buyIn"
            name="buyIn"
            placeholder={CONSTANTS.INPUT.BUY_IN_LABEL}
            {...formik}
          />
        </div>

        <div className="new-game__info-big-game">
          <FormCheckbox
            label={CONSTANTS.INPUT.BIG_GAME_LABEL}
            name="isBigGame"
            id="isBigGame"
            {...formik}
          />
        </div>
      </div>

      <div className="new-game__players">
        <h3>{CONSTANTS.PAGES.PLAYERS}</h3>
        <ul className="new-game__players-list">
          {players.map((pl) => (
            <li key={pl.id} className="new-game__players-list-item">
              <FormCheckbox
                className="mb-2"
                name={'players'}
                id={`${pl.id}`}
                label={pl.name}
                value={pl.id}
                {...formik}
              />
            </li>
          ))}
        </ul>
      </div>
      <div className="form__actions">
        <button type="submit" className="btn btn-primary" disabled={hasError}>
          {CONSTANTS.BUTTON.SAVE}
        </button>
        <CancelButton backTo={CONSTANTS.ROUTE.GAMES} />
      </div>
    </form>
  );
};

export default React.memo(GameForm);
