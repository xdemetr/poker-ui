import cn from 'classnames';
import React from 'react';
import { NavLink, useParams } from 'react-router-dom';
import { useGetGameByName } from '../../../../api/games';
import Alert from '../../../../components/Alert/Alert';
import ProtectedElement from '../../../../components/ProtectedElement/ProtectedElement';
import Spinner from '../../../../components/Spinner/Spinner';
import CONSTANTS from '../../../../general/constants';
import './GameDetails.scss';

const GameDetails = () => {
  const { name } = useParams<{ name: string }>();

  const { data: game, isLoading, error } = useGetGameByName({ name: name! });

  if (isLoading) {
    return <Spinner />;
  }

  if (error && !isLoading) {
    return <Alert type={'danger'} message={error} delay={120} />;
  }

  if (!game?.getGame) {
    return null;
  }

  const groupedResults = game.getGame.players.map((player, idx) => ({ player, result: game.getGame.results[idx] }));

  const renderWinners = groupedResults
    .filter((i) => i.result > 0)
    .sort((a, b) => (a.result < b.result ? 1 : -1))
    .map((winner) => (
      <div className="game-winners__item" key={winner.player.handle}>
        <h4 className="game-winners__name">
          <NavLink to={`${CONSTANTS.ROUTE.PLAYERS}/${winner.player.handle}`}>
            {winner.player.name}
          </NavLink>
        </h4>
        <div className="game-winners__result">+{winner.result}</div>
      </div>
    ));

  const renderItems = groupedResults.map((g, idx) => (
    <tr key={g.player.id || idx}>
      <td width={'15'}>#{idx + 1}</td>
      <td className={cn('', { 'fw-bold': g.result > 0 })}>
        <NavLink to={`${CONSTANTS.ROUTE.PLAYERS}/${g.player.handle}`}>{g.player.name}</NavLink>
      </td>
      <td>{g.result}</td>
    </tr>
  ));

  return (
    <div className="game-details card">
      <header className="card__header">
        <h1>{game.getGame.name}</h1>
        <ProtectedElement classNames="ms-auto">
          <NavLink
            to={`${CONSTANTS.ROUTE.GAMES}/${game.getGame.name}/edit`}
            className="btn btn-outline-primary">
            {game.getGame.results.length === 0 ? CONSTANTS.BUTTON.ADD_RESULT : CONSTANTS.BUTTON.EDIT}
          </NavLink>
        </ProtectedElement>
      </header>

      <article className="card__body">
        <div className="game-winners">
          <h2>{CONSTANTS.PAGES.WINNERS}:</h2>
          <ul className="game-winners__list">{renderWinners}</ul>
        </div>

        <table className="table table_bordered">
          <tbody>{renderItems}</tbody>
        </table>
      </article>
    </div>
  );
};

export default GameDetails;
