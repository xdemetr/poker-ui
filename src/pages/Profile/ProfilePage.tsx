import { useFormik } from 'formik';
import { FormikValues } from 'formik/dist/types';
import React, { useState } from 'react';
import * as Yup from 'yup';
import { graphQLClient } from '../../api/fetcher';
import Alert from '../../components/Alert/Alert';
import FormInput from '../../components/Forms/FormInput';
import PageContainer from '../../components/PageContainer/PageContainer';
import CONSTANTS from '../../general/constants';
import query from '../../graphql/query';

const ProfilePage = () => {
  const [updateSuccess, setUpdateSuccess] = useState(false);
  const [error, setError] = useState('');

  const saveUser = async (profileInput: FormikValues) => {
    try {
      await graphQLClient.request(query.updateUser, { profileInput });
      setUpdateSuccess(true);
      formik.resetForm();
    } catch (e: any) {
      setError(e.response.errors[0].message);
    }
  };

  const localData = JSON.parse(localStorage.getItem('login') as string);

  const UpdateSchema = Yup.object().shape({
    password: Yup.string().required(CONSTANTS.INPUT.MARK_REQUIRED),
    newPassword: Yup.string().required(CONSTANTS.INPUT.MARK_REQUIRED),
    confirmNewPassword: Yup.string().required(CONSTANTS.INPUT.MARK_REQUIRED),
  });

  const formik = useFormik({
    initialValues: {
      id: localData.userId,
      password: '',
      newPassword: '',
      confirmNewPassword: '',
    },
    onSubmit: (values) => {
      saveUser(values);
    },
    validationSchema: UpdateSchema,
  });

  return (
    <PageContainer
      mod="profile-page"
      body={
        <section className="card form form_compact">
          <header className="card__header">
            <h1>{CONSTANTS.PAGES.PROFILE}</h1>
          </header>
          <div className="card__body">
            {updateSuccess && <Alert type={'success'} message="Пароль изменен" delay={10} />}
            {error && <Alert type={'danger'} message={error} delay={10} />}
            <form onSubmit={formik.handleSubmit}>
              <FormInput
                id="password"
                name="password"
                label={CONSTANTS.INPUT.CURRENT_PASSWORD_LABEL}
                type={'password'}
                {...formik}
              />

              <FormInput
                id="newPassword"
                name="newPassword"
                label={CONSTANTS.INPUT.NEW_PASSWORD_LABEL}
                type={'password'}
                {...formik}
              />

              <FormInput
                id="confirmNewPassword"
                name="confirmNewPassword"
                label={CONSTANTS.INPUT.PASSWORD_CONFIRM_LABEL}
                type={'password'}
                {...formik}
              />

              <div className="form__actions">
                <button type="submit" className="btn btn-primary">
                  {CONSTANTS.BUTTON.SAVE}
                </button>
              </div>
            </form>
          </div>
        </section>
      }
    />
  );
};

export default ProfilePage;
