import { useFormik } from 'formik';
import request from 'graphql-request';
import React, { useState } from 'react';
import { Navigate } from 'react-router-dom';
import * as Yup from 'yup';
import { apiUrl, graphQLClient } from '../../api/fetcher';
import { useProfile } from '../../api/profile';
import Alert from '../../components/Alert/Alert';
import FormInput from '../../components/Forms/FormInput';
import PageContainer from '../../components/PageContainer/PageContainer';
import Spinner from '../../components/Spinner/Spinner';
import CONSTANTS from '../../general/constants';
import query from '../../graphql/query';
import jwt from 'jwt-decode';

const AuthPage = () => {
  const [error, setError] = useState('');
  const { data: isAuth, mutate: mutateProfile, isLoading: loading } = useProfile();

  if (isAuth) {
    return <Navigate to={CONSTANTS.ROUTE.GAMES} />;
  }

  const SignInForm = () => {
    const SignupSchema = Yup.object().shape({
      email: Yup.string()
        .email(CONSTANTS.INPUT.EMAIL_WRONG_FORMAT)
        .required(CONSTANTS.INPUT.MARK_REQUIRED),
      password: Yup.string().required(CONSTANTS.INPUT.MARK_REQUIRED),
    });

    const formik = useFormik({
      initialValues: {
        email: '',
        password: '',
      },
      onSubmit: async (values) => {
        try {
          const data = await request(apiUrl, query.login, values);
          const decode: { userId: string, exp: number, isAdmin: boolean } = await jwt(data.login.token);

          localStorage.setItem(
            'login',
            JSON.stringify({
              token: data.login.token,
              userId: decode.userId,
              tokenExpiration: decode.exp,
              isAuth: true,
              isAdmin: decode.isAdmin
            }),
          );
          graphQLClient.setHeader('authorization', data.login.token);
          await mutateProfile({ user: { id: data.login.userId } });

        } catch (e: any) {
          setError(e.response.errors[0].message);
        }
      },
      validationSchema: SignupSchema,
    });
    return (
      <form onSubmit={formik.handleSubmit} className="card form form_compact">
        <header className="card__header">
          <h1>{CONSTANTS.PAGES.AUTH}</h1>
        </header>

        <fieldset className="card__body">
          {loading && <Spinner />}
          {error && <Alert type={'danger'} message={error} delay={20} />}

          <FormInput id="email" name="email" label={CONSTANTS.INPUT.EMAIL_LABEL} {...formik} />

          <FormInput
            id="password"
            name="password"
            label={CONSTANTS.INPUT.PASSWORD_LABEL}
            type={'password'}
            {...formik}
          />

          <div className="form__actions">
            <button className="btn btn-lg btn-block btn-primary" type="submit">
              {CONSTANTS.BUTTON.SIGN_IN}
            </button>
          </div>
        </fieldset>
      </form>
    );
  };

  return <PageContainer mod="auth-page" body={<SignInForm />} />;
};

export default AuthPage;
