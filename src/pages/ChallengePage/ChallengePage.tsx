import moment from 'moment';
import { useState } from 'react';
import { useGetStatByYear } from '../../api/players';
import ChallengeTable from '../../components/ChallengeTable/ChallengeTable';
import PageContainer from '../../components/PageContainer/PageContainer';
import Spinner from '../../components/Spinner/Spinner';
import './ChallengePage.scss';

const ChallengePage = () => {
  const [currentYear, setCurrentYear] = useState(moment().year());
  const { data: stat, isLoading } = useGetStatByYear(currentYear);

  const formattedPlayers = stat?.getStatByYear.map(player => {
    // @ts-ignore
    const totalResult = player.results.reduce((sum, game) => sum + game.result, 0);
    return { ...player, totalResult };
  }).sort((a, b) => b.totalResult - a.totalResult);

  const startYear = 2017;
  const cYear = new Date().getFullYear();
  const years = [];

  for (let year = cYear; year>= startYear; year--) {
    years.push(year);
  }

  if (isLoading) {
    return <Spinner />;
  }

  if (!formattedPlayers) {
    return null;
  }

  return (
    <PageContainer
      mod="challenge-page"
      body={
        <div className="card">
          <header className="card__header challenge-page__header">
            <h2 className="list-group__title">Статистика за</h2>
              <select className="form-select" onChange={e => setCurrentYear(Number(e.target.value))} value={currentYear}>
                {years.map((year) => <option key={`y-${year}`}>{year}</option>)}
              </select>
          </header>
          <div className="card__body">
            <ChallengeTable players={formattedPlayers} />
          </div>
        </div>
      }
    />
  );
};

export default ChallengePage;
