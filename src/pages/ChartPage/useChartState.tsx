import { useFormik } from 'formik';
import moment from 'moment';
import { useEffect } from 'react';
import { useGetPlayers } from '../../api/players';
import { PlayerHistory, PlayerHistoryRender } from '../../general/types/Player';

interface Props {
  data: PlayerHistory[];
  setSelectedPlayers: (pl: string[]) => void;
}

const useChartState = ({ data, setSelectedPlayers }: Props) => {
  const { data: players } = useGetPlayers();

  const formik = useFormik({
    initialValues: {
      players: [''],
    },
    enableReinitialize: true,
    onSubmit: (values) => {
      setSelectedPlayers(values.players.filter((i) => i));
    },
  });

  useEffect(() => {
    const defaultSelectedPlayers = ['azat', 'max', 'renat', 'marat', 'dima', 'tagir', 'chaika', 'roman', 'kastet'];
    if (formik.values.players.length <= 1) {
      formik.setFieldValue('players', defaultSelectedPlayers);
      setSelectedPlayers(defaultSelectedPlayers);
    }
  }, [formik, setSelectedPlayers]);

  const dates = Array.from(new Set(data.map((item: PlayerHistory) => item.date))).sort((a, b) =>
    moment(a).isAfter(b) ? 1 : -1,
  );
  const names = Array.from(new Set(data.map((item) => item.name)));

  const groupedByDate = dates.reduce((acc: PlayerHistory[][], el: string, idx: number) => {
    const playerByDate = data.filter((i) => i.date === el);
    if (playerByDate.length !== names.length) {
      names.forEach((name) => {
        const findedPlayer = playerByDate.filter((i) => i.name !== name);
        if (findedPlayer.length === playerByDate.length) {
          let prevValue;
          if (idx >= 1) {
            prevValue = acc[idx - 1].find((el: any) => el.name === name);
          }
          playerByDate.push({ date: el, value: prevValue?.value || 0, name: name });
        }
      });
    }
    acc.push(playerByDate);
    return acc;
  }, []);

  const mm = groupedByDate.reduce((acc: PlayerHistoryRender[], el: PlayerHistory[]) => {
    const element = el.reduce((accDate: PlayerHistoryRender, elDate) => {
      accDate.date = moment(elDate.date).format('DD-MM-YYYY');
      accDate[elDate.name] = elDate.value;
      return accDate;
    }, {});
    acc.push(element);
    return acc;
  }, []);

  const firstEl = names.reduce((a, v) => ({ ...a, [v]: 0 }), {});

  const mergedData = [firstEl, ...mm];

  const colors = [
    '#003f5c',
    '#a05195',
    '#21f00e',
    '#f5a742',
    '#f54242',
    '#42f2f5',
    '#0e74f0',
    '#5b8a15',
    '#d45087',
  ];

  return {
    mergedData,
    colors,
    names,
    formik,
    players: players?.getAllPlayers,
  };
};

export default useChartState;
