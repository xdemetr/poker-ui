import React, { useState } from 'react';
import { Brush, CartesianGrid, Legend, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';
import { useGetPlayersHistory } from '../../api/players';
import FormCheckbox from '../../components/Forms/FormCheckbox';
import PageContainer from '../../components/PageContainer/PageContainer';
import CONSTANTS from '../../general/constants';
import './ChartPage.scss';
import useChartState from './useChartState';

const ChartPage = () => {
  const [selectedPlayers, setSelectedPlayers] = useState<string[]>([]);
  const [showTooltip, setShowTooltip] = useState<boolean>(false);

  const { data: playerHistory } = useGetPlayersHistory(selectedPlayers);
  const data = playerHistory?.getPlayersHistory || [];

  const { colors, mergedData, names, formik, players } = useChartState({
    data,
    setSelectedPlayers,
  });

  if (!players) {
    return null;
  }

  return (
    <PageContainer
      mod="chart-page"
      size="wide"
      body={
        <>
          <div className="chart-page__content">
            <ResponsiveContainer width="100%" height="100%">
              <LineChart width={500} height={500} data={mergedData}>
                <CartesianGrid strokeDasharray="1 1" />
                <XAxis dataKey="date" style={{ fontSize: '10px' }} />
                <YAxis width={20} style={{ fontSize: '10px' }} />
                {showTooltip && <Tooltip />}
                <Legend />
                {names[0] && (
                  <Line type="monotone" dot={false} dataKey={names[0]} stroke={colors[0]} />
                )}
                {names[1] && (
                  <Line type="monotone" dot={false} dataKey={names[1]} stroke={colors[1]} />
                )}
                {names[2] && (
                  <Line type="monotone" dot={false} dataKey={names[2]} stroke={colors[2]} />
                )}
                {names[3] && (
                  <Line type="monotone" dot={false} dataKey={names[3]} stroke={colors[3]} />
                )}
                {names[4] && (
                  <Line type="monotone" dot={false} dataKey={names[4]} stroke={colors[4]} />
                )}
                {names[5] && (
                  <Line type="monotone" dot={false} dataKey={names[5]} stroke={colors[5]} />
                )}
                {names[6] && (
                  <Line type="monotone" dot={false} dataKey={names[6]} stroke={colors[6]} />
                )}
                {names[7] && (
                  <Line type="monotone" dot={false} dataKey={names[7]} stroke={colors[7]} />
                )}
                {names[8] && (
                  <Line type="monotone" dot={false} dataKey={names[8]} stroke={colors[8]} />
                )}
                <Brush />
              </LineChart>
            </ResponsiveContainer>
          </div>
          <div className="chart-page__settings container">
            <form onSubmit={formik.handleSubmit}>
              <div className="chart-page__settings-list">
                {players.map((pl, idx) => (
                  <div key={pl.id} className="chart-page__settings-list-item">
                    <FormCheckbox
                      className="mb-2"
                      name={'players'}
                      id={`${pl.id}`}
                      label={pl.name}
                      value={pl.handle}
                      checked={formik.values.players.includes(pl.handle)}
                      {...formik}
                    />
                  </div>
                ))}
              </div>

              <div className="form__actions">
                <button className="btn btn-primary me-4">{CONSTANTS.BUTTON.DRAW_CHART}</button>
              </div>

              <div className="form__actions">
                <div className="form-check form-switch">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    id="showTooltip"
                    checked={showTooltip}
                    onChange={() => setShowTooltip(!showTooltip)}
                  />
                  <label className="form-check-label" htmlFor="showTooltip">
                    Подсказки
                  </label>
                </div>
              </div>
            </form>
          </div>
        </>
      }
    />
  );
};
export default ChartPage;
