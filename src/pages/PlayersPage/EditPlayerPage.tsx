import React, { useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { graphQLClient } from '../../api/fetcher';
import { useGetPlayerByHandle } from '../../api/players';
import PageContainer from '../../components/PageContainer/PageContainer';
import CONSTANTS from '../../general/constants';
import { IPlayer } from '../../general/types/Player';
import query from '../../graphql/query';
import PlayerForm from './components/PlayerForm/PlayerForm';

const EditPlayerPage = () => {
  const { handle } = useParams<{ handle: string }>();

  const { data: player } = useGetPlayerByHandle(handle!);
  const [error, setError] = useState('');

  const navigate = useNavigate();

  const handleSavePlayer = async (player: IPlayer) => {
    try {
      const data = await graphQLClient.request(query.createPlayer, { playerInput: player });
      navigate(`${CONSTANTS.ROUTE.PLAYERS}/${data?.createPlayer.handle}`);

    } catch (e: any) {
      setError(e.response.errors[0].message);
    }
  };

  const body = (
    <>
      <PlayerForm
        player={player?.getPlayerByHandle}
        handleSave={handleSavePlayer}
        errorMessage={error}
      />
    </>
  );

  return <PageContainer mod="edit-player-page" body={body} />;
};

export default React.memo(EditPlayerPage);
