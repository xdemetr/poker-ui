import React from 'react';
import { useFormik } from 'formik';
import { IPlayer } from '../../../../general/types/Player';
import FormInput from '../../../../components/Forms/FormInput';
import FormCheckbox from '../../../../components/Forms/FormCheckbox';
import CONSTANTS from '../../../../general/constants';
import CancelButton from '../../../../components/Forms/CancelButton';
import Alert from '../../../../components/Alert/Alert';

interface Props {
  player: IPlayer | null | undefined;
  handleSave: (player: IPlayer, resetHandler: () => void) => void;
  errorMessage?: string;
}

const PlayerForm: React.FC<Props> = ({ player, handleSave, errorMessage }) => {
  const formik = useFormik({
    initialValues: {
      name: player?.name || '',
      handle: player?.handle || '',
      isRegular: player?.isRegular || false,
      isShowInRating: player?.isShowInRating || false,
      id: player?.id || null,
    },
    enableReinitialize: true,
    onSubmit: (values, { resetForm }) => {
      handleSave(values as IPlayer, resetForm);
    },
  });

  const backUrl = player?.handle
    ? `${CONSTANTS.ROUTE.PLAYERS}/${player.handle}`
    : CONSTANTS.ROUTE.PLAYERS;
  const title = player?.handle ? CONSTANTS.PAGES.EDIT : CONSTANTS.PAGES.PLAYER_NEW;

  return (
    <section className="player-form card form form_compact">
      <header className="card__header">
        <h1>{title}</h1>
      </header>

      <div className="card__body">
        {errorMessage && <Alert type={'danger'} message={errorMessage} delay={10} />}

        <form onSubmit={formik.handleSubmit}>
          <FormInput id="name" name="name" label={CONSTANTS.INPUT.PLAYER_NAME} {...formik} />

          <FormInput id="handle" name="handle" label={CONSTANTS.INPUT.PLAYER_HANDLER} {...formik} />

          <div className="form-group">
            <FormCheckbox
              label={CONSTANTS.INPUT.PLAYER_IS_REGULAR}
              className="mb-2"
              name="isRegular"
              id="isRegular"
              checked={formik.values.isRegular}
              {...formik}
            />
          </div>

          <div className="form-group">
            <FormCheckbox
              name="isShowInRating"
              label={CONSTANTS.INPUT.PLAYER_SHOW_IN_RATING}
              className="mb-2"
              id="isShowInRating"
              checked={formik.values.isShowInRating}
              {...formik}
            />
          </div>

          <div className="form__actions">
            <button type="submit" className="btn btn-primary">
              {CONSTANTS.BUTTON.SAVE}
            </button>

            <CancelButton backTo={backUrl} />
          </div>
        </form>
      </div>
    </section>
  );
};

export default PlayerForm;
