import React from 'react';
import { NavLink } from 'react-router-dom';
import { CartesianGrid, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';
import { useGetPlayerByHandle, useGetPlayerHistory } from '../../../../api/players';
import Alert from '../../../../components/Alert/Alert';
import ProtectedElement from '../../../../components/ProtectedElement/ProtectedElement';
import Spinner from '../../../../components/Spinner/Spinner';
import CONSTANTS from '../../../../general/constants';
import PlayerGamesInfo from './PlayerGamesInfo';
import PlayerGamesResults from './PlayerGamesResults';

interface Props {
  handle: string;
}

const PlayerDetails: React.FC<Props> = ({ handle }) => {

  const { data: playerHistory, isLoading: loadingPlayerHistory } = useGetPlayerHistory(handle);
  const { data: player, isLoading: loadingPlayer, error } = useGetPlayerByHandle(handle);


  if (loadingPlayer || loadingPlayerHistory) {
    return <Spinner />;
  }

  if (error) {
    return <Alert type={'danger'} message={error} />;
  }


  if (!player?.getPlayerByHandle) {
    return null;
  }

  const history = [
    {
      value: 0,
    },
    // @ts-ignore
    ...playerHistory?.getPlayerHistory,
  ];

  return (
    <div className="player-details card">
      <header className="card__header">
        <h1>{player.getPlayerByHandle.name}</h1>
        {player.getPlayerByHandle.isRegular && (
          <span className="badge badge_info">{CONSTANTS.INPUT.PLAYER_IS_REGULAR}</span>
        )}

        <ProtectedElement classNames="ms-auto">
          <NavLink
            className="btn btn-outline-primary"
            to={`${CONSTANTS.ROUTE.PLAYERS}/${player.getPlayerByHandle.handle}/edit`}>
            {CONSTANTS.BUTTON.EDIT}
          </NavLink>
        </ProtectedElement>
      </header>

      <section className="card__body">
        {playerHistory?.getPlayerHistory && (
          <div style={{ height: '300px' }}>
            <ResponsiveContainer width="100%" height="100%">
              <LineChart width={500} height={500} data={history}>
                <CartesianGrid strokeDasharray="3 3" stroke="#2D3819" />
                <XAxis dataKey="date" style={{ fontSize: '8px', color: '#000' }} />
                <YAxis width={20} style={{ fontSize: '10px' }} />
                <Tooltip />
                <Line
                  type="monotone"
                  dataKey="value"
                  stroke="#aaa"
                  dot={false}
                  activeDot={{ r: 8 }}
                />
              </LineChart>
            </ResponsiveContainer>
          </div>
        )}
        <PlayerGamesInfo player={player.getPlayerByHandle} />
        <PlayerGamesResults results={player.getPlayerByHandle.results} />
      </section>
    </div>
  );
};

export default PlayerDetails;
