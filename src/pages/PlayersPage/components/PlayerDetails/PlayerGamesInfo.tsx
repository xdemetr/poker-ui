import cn from 'classnames';
import React from 'react';
import { useGetGameCount } from '../../../../api/games';
import Alert from '../../../../components/Alert/Alert';
import CONSTANTS from '../../../../general/constants';
import { IPlayer } from '../../../../general/types/Player';
import PlayerStatChart from './PlayerStatChart';

interface Props {
  player: IPlayer;
}

const PlayerGamesInfo: React.FC<Props> = ({ player }) => {
  const { data: gameCount } = useGetGameCount();

  if (!gameCount?.getGameCount || !player) {
    return null;
  }

  if (player.gameCount === 0) {
    return (
      <Alert type="success" message="У игрока еще нет статистики." delay={5000} />
    );
  }

  return (
    <div className="player-stat">
      <div className="player-stat__column">
        <ul className="player-stat__group stat-group">
          <li className="stat-group__item">
            <span className="stat-group__label">{CONSTANTS.PAGES.BALANCE}</span>
            <div className="stat-group__value">
              <span className="badge badge_info">{player.balance}</span>
            </div>
          </li>

          <li className="stat-group__item">
            <span className="stat-group__label">{CONSTANTS.PAGES.ATTENDANCE}</span>
            <div className="stat-group__value">
              <span
                className="badge badge_info">{player.attendance}%</span> {player.gameCount} из {gameCount.getGameCount}
            </div>
          </li>

          <li className="stat-group__item">
            <span className="stat-group__label">{CONSTANTS.PAGES.AVG_PER_GAME}</span>
            <div className="stat-group__value">
              <span
                className={cn('badge', {
                  badge_success: player.balance > 0,
                  badge_danger: player.balance < 0,
                })}>
                {Math.floor(player.balance / player.gameCount)}
              </span>
            </div>
          </li>
        </ul>
      </div>

      <div className="player-stat__column">
        <ul className="player-stat__group stat-group">
          <li className="stat-group__item">
            <span className="stat-group__label">{CONSTANTS.PAGES.SERIES_OF_WIN}</span>
            <div className="stat-group__value">
              <span className="badge badge_success">{player.maxSeriesOfWin}</span>
            </div>
          </li>

          <li className="stat-group__item">
            <span className="stat-group__label">{CONSTANTS.PAGES.SERIES_OF_LOOSE}</span>
            <div className="stat-group__value">
              <span className="badge badge_danger">{player.maxSeriesOfLoose}</span>
            </div>
          </li>

          <li className="stat-group__item">
            <PlayerStatChart
              showLegend={true}
              winCount={player.results?.filter((g) => g.result >= 0).length || 0}
              looseCount={player.results?.filter((g) => g.result < 0).length || 0}
            />
          </li>
        </ul>
      </div>
    </div>
  );
};

export default React.memo(PlayerGamesInfo);
