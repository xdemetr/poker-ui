import React, { useState } from 'react';
import cn from 'classnames';
import { NavLink } from 'react-router-dom';
import { IGamePreview } from '../../../../general/types/Game';
import CONSTANTS from '../../../../general/constants';
import { ReactComponent as PlusIcon } from '../../../../assets/images/plus.svg';
import { ReactComponent as MinusIcon } from '../../../../assets/images/minus.svg';
import PlayerStatChart from './PlayerStatChart';

interface Props {
  result: {
    year: string;
    result: number;
    games: IGamePreview[];
  };
  isCollapsed: boolean;
}

const PlayerResultsByYear: React.FC<Props> = ({ result, isCollapsed = true }) => {
  const [collapsed, setCollapsed] = useState(isCollapsed);

  const handleToggle = () => {
    setCollapsed(!collapsed);
  };

  const renderResults = result.games.map((game) => (
    <tr
      key={game.id}
      className={cn({ table_danger: game.result < 0 }, { table_success: game.result > 0 })}>
      <td>
        <NavLink to={`${CONSTANTS.ROUTE.GAMES}/${game.name}`}>{game.name}</NavLink>
      </td>
      <td>{game.result}</td>
    </tr>
  ));

  return (
    <>
      <section className={cn('player-results', { 'player-results_collapsed': collapsed })}>
        <header className="player-results__header" onClick={handleToggle}>
          <span className="player-results__header-icon">
            {collapsed ? <PlusIcon /> : <MinusIcon />}
          </span>
          <span className="player-results__header-title">Результаты за {result.year} год</span>
          <span
            className={cn(
              'badge ms-auto player-results__header-result',
              { badge_danger: result.result < 0 },
              { badge_success: result.result > 0 },
            )}>
            {result.result}
          </span>
        </header>

        <div className="player-results__body">
          <table className={cn('table table-hover')}>
            <thead className="thead-light">
              <tr>
                <th colSpan={2}>
                  <PlayerStatChart
                    showLegend={true}
                    winCount={result.games.filter((g) => g.result >= 0).length || 0}
                    looseCount={result.games.filter((g) => g.result < 0).length || 0}
                  />
                </th>
              </tr>
              <tr>
                <th style={{ width: '50%' }}>Дата</th>
                <th>Результат</th>
              </tr>
            </thead>
            <tbody>{renderResults}</tbody>
          </table>
        </div>
      </section>
    </>
  );
};

export default React.memo(PlayerResultsByYear);
