import React from 'react';
import PlayerResultsByYear from './PlayerResultsByYear';
import { GameResult } from '../../../../general/types/GameResult';
import { IGamePreview } from '../../../../general/types/Game';

interface Props {
  results: GameResult[] | undefined;
}

interface GroupProps {
  [key: string]: IGamePreview[];
}

const PlayerGamesResults: React.FC<Props> = ({ results }) => {
  if (!results) {
    return null;
  }

  const resultsGroupedByYear = results.reduce((group, result) => {
    const year = result.game.name.slice(-4);

    if (!group[year]) {
      group[year] = [];
    }

    group[year].push({ ...result.game, result: result.result });
    return group;
  }, {} as GroupProps);

  const groupArrays = Object.keys(resultsGroupedByYear)
    .map((year) => {
      const resByYear = resultsGroupedByYear[year].reduce((acc, item) => {
        return acc + item.result;
      }, 0);

      return {
        year,
        games: resultsGroupedByYear[year],
        result: resByYear,
      };
    })
    .sort((a, b) => (a.year < b.year ? 1 : -1));

  const renderItems = groupArrays.map((group, idx) => (
    <PlayerResultsByYear result={group} key={group.year} isCollapsed={idx !== 0} />
  ));

  return <div className="player-games-results">{renderItems}</div>;
};

export default React.memo(PlayerGamesResults);
