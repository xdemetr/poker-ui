import React from 'react';
import CONSTANTS from '../../../../general/constants';

interface IProps {
  showLegend: boolean;
  winCount: number;
  looseCount: number;
}

const PlayerStatChart: React.FC<IProps> = ({ showLegend, winCount, looseCount }) => {
  const totalGameCount = winCount + looseCount;
  const winPercent = Math.round((winCount / totalGameCount) * 100);
  const loosePercent = Math.round((looseCount / totalGameCount) * 100);

  return (
    <div className="player-chart">
      <div className="player-chart__content">
        {winPercent > 0 && (
          <div
            style={{ width: winPercent + '%' }}
            className="player-chart__value player-chart__win">
            {winPercent}%
          </div>
        )}
        <div
          style={{ width: loosePercent + '%' }}
          className="player-chart__value player-chart__loose">
          {loosePercent}%
        </div>
      </div>

      {showLegend && (
        <div className="player-chart__legend">
          <span>
            {CONSTANTS.PAGES.TOTAL_GAMES}:&nbsp;{totalGameCount}
          </span>
          <span>
            {CONSTANTS.PAGES.WINS}:&nbsp;{winCount}
          </span>
          <span>
            {CONSTANTS.PAGES.LOOSES}:&nbsp;{looseCount}
          </span>
        </div>
      )}
    </div>
  );
};

export default PlayerStatChart;
