import cn from 'classnames';
import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { useGetPlayersFull } from '../../../../api/players';
import Alert from '../../../../components/Alert/Alert';
import Spinner from '../../../../components/Spinner/Spinner';
import CONSTANTS from '../../../../general/constants';
import { IPlayer } from '../../../../general/types/Player';
import { ReactComponent as SortIcon } from '../../../../assets/images/sort.svg';

const PlayersList = () => {
  type sortValues = 'balance' | 'gameCount' | 'maxSeriesOfWin' | 'maxSeriesOfLoose';

  const [sort, setSort] = useState<sortValues>('balance');
  const { data: players, isLoading, error } = useGetPlayersFull('balance');
  const [sortDir, setSortDir] = useState<'asc' | 'desc'>('asc');

  const [showHidden, setShowHidden] = useState(false);

  const [renderedPlayers, setRenderedPlayers] = useState<IPlayer[] | undefined>([]);
  const [hiddenPlayers, setHiddenPlayers] = useState<IPlayer[] | []>([]);


  useEffect(() => {
    const allPlayers = players?.getAllPlayers;
    allPlayers?.sort((a, b) => a.balance < b.balance ? 1 : -1);

    const activePlayers = players?.getAllPlayers.filter(pl => pl.isShowInRating);
    const hiddenPlayers = players?.getAllPlayers.filter(pl => !pl.isShowInRating);

    setRenderedPlayers(showHidden ? allPlayers : activePlayers);
    setHiddenPlayers(hiddenPlayers!);
  }, [players?.getAllPlayers, showHidden]);

  if (isLoading) {
    return <Spinner />;
  }

  if (error) {
    return <Alert type={'warning'} message={error.message} />;
  }

  const sortClickHandle = (sortBy: sortValues) => {
    if (sortBy !== sort) {
      setSort(sortBy);
      setSortDir('asc');
    }

    let sDir = 1;

    if (sortDir === 'asc' && sortBy === sort) {
      setSortDir('desc');
      sDir = -1;
    } else {
      setSortDir('asc');
      sDir = 1;
    }

    const arr = [...renderedPlayers!].sort((a, b) => a[sortBy] < b[sortBy] ? sDir : sDir * -1);

    setRenderedPlayers(arr);
  };

  const handleShowHidden = () => {
    setShowHidden(!showHidden);
  };

  const checkSortIsActive = (item: string) => item === sort;

  if (!renderedPlayers) {
    return null;
  }

  const renderItems = renderedPlayers.map((pl, idx) => (
    <tr key={pl.id} className={cn({ 'table-warning': !pl.isShowInRating })}>
      <td width="10">{idx + 1}</td>
      <td>
        <NavLink to={`${CONSTANTS.ROUTE.PLAYERS}/${pl.handle}`}>{pl.name}</NavLink>
      </td>
      <td className="center">{pl.balance}</td>
      <td width="14%" className="center">{pl.gameCount}</td>
      <td width="14%" className="center">{pl.maxSeriesOfWin}</td>
      <td width="14%" className="center">{pl.maxSeriesOfLoose}</td>
    </tr>
  ));

  const renderSortButton = (name: sortValues, title: string) => {
    return (
      <button
        className={cn('tabs__item', {
          'tabs__item_active': checkSortIsActive(name),
        })}
        onClick={() => sortClickHandle(name)}>
        {title}

        {sort === name && (
          <SortIcon className={cn('tabs__icon', {
            'tabs__icon_asc': sortDir === 'asc',
            'tabs__icon_desc': sortDir === 'desc',
          })} />
        )}
      </button>
    );
  };

  return (
    <div className="players-list">
      <section className="card">
        <header className="card__header">
          <h1>{CONSTANTS.PAGES.PLAYERS}</h1>
        </header>

        <div className="card__body">
          <div className="players-list__sorting">
            <div className="tabs">
              {renderSortButton('balance', 'Баланс')}
              {renderSortButton('gameCount', 'Игры')}
              {renderSortButton('maxSeriesOfWin', 'Макс. серия побед')}
              {renderSortButton('maxSeriesOfLoose', 'Макс. серия поражений')}
            </div>
          </div>

          <div className="table-responsive">
            <table className="table table-bordered table-hover">
              <colgroup>
                <col id="id" />
                <col id="name" />
                <col id="balance" className={sort === 'balance' ? 'table_highlight' : ''} />
                <col id="gameCount" className={sort === 'gameCount' ? 'table_highlight' : ''} />
                <col id="maxSeriesOfWin" className={sort === 'maxSeriesOfWin' ? 'table_highlight' : ''} />
                <col id="maxSeriesOfLoose" className={sort === 'maxSeriesOfLoose' ? 'table_highlight' : ''} />
              </colgroup>
              <thead className="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Имя</th>
                <th scope="col" className="center">Баланс</th>
                <th scope="col" className="center">Игры</th>
                <th scope="col" className="center">max win</th>
                <th scope="col" className="center">max loose</th>
              </tr>
              </thead>

              <tbody>{renderItems}</tbody>
            </table>
          </div>

          {hiddenPlayers.length > 0 && (
            <div className="players-list__footer">
              В рейтинге не отображаются скрытые игроки.
              <span className="btn btn-outline-primary" onClick={handleShowHidden}>
                {showHidden ? 'Скрыть' : 'Показать'}
              </span>
            </div>
          )}
        </div>
      </section>
    </div>
  );
};

export default PlayersList;
