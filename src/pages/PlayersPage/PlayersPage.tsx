import React from 'react';
import { NavLink, useParams } from 'react-router-dom';
import { useGetPlayers } from '../../api/players';
import ContentList from '../../components/ContentList/ContentList';
import PageContainer from '../../components/PageContainer/PageContainer';
import ProtectedElement from '../../components/ProtectedElement/ProtectedElement';
import CONSTANTS from '../../general/constants';
import PlayerDetails from './components/PlayerDetails/PlayerDetails';
import PlayersList from './components/PlayersList/PlayersList';
import './player-page.scss';

const PlayersPage = () => {
  const { handle } = useParams<{ handle: string }>();

  const { data: players } = useGetPlayers();

  const bodyComponent = handle ? <PlayerDetails handle={handle} /> : <PlayersList />;

  if (!players?.getAllPlayers) {
    return null;
  }

  return (
    <PageContainer
      mod="players-page"
      aside={
        <>
          <ProtectedElement classNames="action-button">
            <NavLink
              to={`${CONSTANTS.ROUTE.PLAYERS}/new`}
              className="btn d-block btn-outline-primary btn-lg">
              {CONSTANTS.BUTTON.NEW_PLAYER}
            </NavLink>
          </ProtectedElement>

          <ContentList
            content={players.getAllPlayers}
            type="player"
            loading={false}
            title={CONSTANTS.PAGES.PLAYERS}
          />
        </>
      }
      body={bodyComponent}
    />
  );
};

export default PlayersPage;
