import { GameResult } from './GameResult';
import { BaseItem } from './BaseItem';

export interface IPlayer extends BaseItem {
  balance: number;
  gameCount: number;
  handle: string;
  isRegular: boolean;
  isShowInRating: boolean;
  maxSeriesOfLoose: number;
  maxSeriesOfWin: number;
  results?: GameResult[];
  attendance: number
}

export interface PlayerHistory {
  date: string;
  value: number;
  name: string;
}

export interface PlayerHistoryRender {
  [key: string]: string | number;
}
