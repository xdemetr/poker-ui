export interface ITokenJWT {
  email: string;
  success?: boolean;
  token: string;
  tokenExpiration: number;
  exp: number;
}
