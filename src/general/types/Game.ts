import { IPlayer } from './Player';
import { BaseItem } from './BaseItem';

export interface IGame extends BaseItem {
  players: IPlayer[];
  results: number[];
  handle?: string;
  date: Date;
  buyIn: number;
  isBigGame: boolean;
}

export interface IGamePreview extends BaseItem {
  date: string;
  isBigGame: boolean;
  result: number;
}

interface PagerItem {
  number: number;
  url: string;
}

export interface PageResponse<T> {
  data: T[];
  has_more: boolean;
  itemCount: number;
  pageCount: number;
  pages: [{ number: number; url: string }];
}
