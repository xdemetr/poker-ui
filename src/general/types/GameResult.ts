import { IGamePreview } from './Game';

export interface GameResult {
  game: IGamePreview;
  result: number;
  id: string;
}
