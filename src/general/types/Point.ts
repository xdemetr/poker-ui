export interface Point {
  _id: string;
  name: string;
  position: number[];
  order: number;
  user?: string;
}
