import cn from 'classnames';
import ru from 'date-fns/locale/ru';
import { useField, useFormikContext } from 'formik';
import React from 'react';
import DatePicker, { registerLocale, setDefaultLocale } from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';
import './datepicker.css';

registerLocale('ru-RU', { ...ru, options: { ...ru.options, weekStartsOn: 0 } });
setDefaultLocale('ru-RU');

const FormDatePicker2 = (props: any) => {
  const { setFieldValue } = useFormikContext();
  const [field] = useField(props);

  return (
    // @ts-ignore
    <DatePicker
      {...field}
      {...props}
      dateFormat="dd-MM-yyyy"
      selected={(field.value && new Date(field.value)) || null}
      className={cn('form-control')}
      autoComplete={'off'}
      locale={ru}
      onChange={(val) => setFieldValue(field.name, val)}
    />
  );
};

export default FormDatePicker2;
