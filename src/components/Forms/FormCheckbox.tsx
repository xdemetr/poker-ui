import React, { ChangeEventHandler } from 'react';
import cn from 'classnames';
import { FormikErrors, FormikTouched } from 'formik';
import { FormikValues } from 'formik/dist/types';

interface Props {
  handleChange: ChangeEventHandler;
  values: FormikValues;
  touched: FormikTouched<FormikValues>;
  errors: FormikErrors<FormikValues>;
  name: string;
  id: string;
  label?: string;
  className?: string;
  value?: string;
  checked?: boolean;
}

const FormCheckbox: React.FC<Props> = ({
  handleChange,
  values,
  touched,
  errors,
  name,
  id,
  label,
  className,
  value,
  checked,
}) => {
  const hasError = !!(errors[name] && touched[name]);
  const controlClass = cn(`${className} form-check`);
  return (
    <div className={controlClass}>
      <input
        type="checkbox"
        className="form-check-input"
        id={id}
        name={name}
        onChange={handleChange}
        value={value}
        checked={checked}
      />

      <label className="form-check-label" htmlFor={id}>
        {label}
      </label>

      {hasError && <div className="invalid-feedback">{String(errors[name])}</div>}
    </div>
  );
};

export default FormCheckbox;
