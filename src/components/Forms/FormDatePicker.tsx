import React from 'react';
import cn from 'classnames';
import DatePicker from 'react-datepicker';
import { FormInputProps } from './FormInput';

import 'react-datepicker/dist/react-datepicker.css';
import './datepicker.css';

import { registerLocale, setDefaultLocale } from 'react-datepicker';
import ru from 'date-fns/locale/ru';

// registerLocale('ru', ru);
registerLocale('ru-RU', { ...ru, options: { ...ru.options, weekStartsOn: 0 } });
setDefaultLocale('ru-RU');

interface Props extends Omit<FormInputProps, 'handleChange'> {
  handleChange: any; //(n: string, v: Date) => void;
}

const FormDatePicker: React.FC<Props> = ({
  handleChange,
  name,
  label,
  errors,
  values,
  touched,
  value,
}) => {
  const hasError = !!(errors[name] && touched[name]);

  // values[name] = Date.now()

  return (
    <div
      className={cn('datepicker form-group mb-0 d-flex align-items-center', {
        'has-danger': hasError,
      })}>
      {label && (
        <label htmlFor={name} className="mb-0 me-2">
          {label}
        </label>
      )}
      {/*// @ts-ignore*/}
      <DatePicker
        dateFormat="dd-MM-yyyy"
        className={cn('form-control', { 'is-invalid': hasError })}
        wrapperClassName={'flex-grow-1'}
        name={name}
        id={name}
        locale="ru"
        value={values[name]}
        autoComplete={'off'}
        selected={values[name]}
        onChange={(date: Date | null) => handleChange(name, date)}
      />
      {hasError && <div className="invalid-feedback">{String(errors[name])}</div>}
    </div>
  );
};

export default FormDatePicker;
