import React, { ChangeEventHandler, FocusEventHandler } from 'react';
import cn from 'classnames';
import { FormikErrors, FormikTouched } from 'formik';
import { FormikValues } from 'formik/dist/types';

export interface FormInputProps {
  handleChange: ChangeEventHandler;
  handleBlur: FocusEventHandler;
  values: FormikValues;
  touched: FormikTouched<FormikValues>;
  errors: FormikErrors<FormikValues>;
  name: string;
  id: string;
  label?: string;
  placeholder?: string;
  type?: string;
  autoComplete?: string | undefined;
  value?: string;
}

const FormInput: React.FC<FormInputProps> = ({
  handleChange,
  handleBlur,
  values,
  touched,
  errors,
  name,
  id,
  label,
  placeholder,
  type,
  autoComplete,
  value,
}) => {
  const hasError = !!(errors[name] && touched[name]);

  return (
    <div className={cn('form-group', { 'has-danger': hasError })}>
      {label && <label htmlFor={id}>{label}</label>}

      <input
        className={cn('form-control', { 'is-invalid': hasError })}
        name={name}
        id={id}
        placeholder={placeholder}
        onChange={handleChange}
        onBlur={handleBlur}
        type={type ? type : 'text'}
        autoComplete={autoComplete}
        // value={value || values[name]}
        value={values[name]}
      />

      {hasError && <div className="invalid-feedback">{String(errors[name])}</div>}
    </div>
  );
};

export default FormInput;
