import React from 'react';
import CONSTANTS from '../../general/constants';
import { useNavigate } from 'react-router-dom';

interface Props {
  backTo: string;
}

const CancelButton: React.FC<Props> = ({ backTo }) => {
  const navigate = useNavigate();

  return (
    <button type="button" className="btn" onClick={() => navigate(backTo)}>
      {CONSTANTS.BUTTON.CANCEL}
    </button>
  );
};

export default CancelButton;
