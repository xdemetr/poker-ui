import React from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import useBreadcrumbs from 'use-react-router-breadcrumbs';
import CONSTANTS from '../../general/constants';

const Breadcrumbs = () => {
  const location = useLocation();
  const routeConfig = [
    {
      path: CONSTANTS.ROUTE.HOME,
      breadcrumb: CONSTANTS.PAGES.HOME,
    },
    {
      path: CONSTANTS.ROUTE.LOGIN,
      breadcrumb: CONSTANTS.PAGES.AUTH,
    },
    {
      path: CONSTANTS.ROUTE.CHART,
      breadcrumb: CONSTANTS.PAGES.CHART,
    },
    {
      path: CONSTANTS.ROUTE.PLAYERS,
      breadcrumb: CONSTANTS.PAGES.PLAYERS,
    },
    {
      path: CONSTANTS.ROUTE.GAMES,
      breadcrumb: CONSTANTS.PAGES.GAMES,
    },
  ];
  const breadcrumbs = useBreadcrumbs(routeConfig);

  if (location.pathname === CONSTANTS.ROUTE.HOME) {
    return null;
  }

  return (
    <div className="container">
      <ol className="breadcrumb">
        {breadcrumbs.map(({ match, breadcrumb }, idx) => (
          <li key={match.url} className="breadcrumb-item">
            {idx + 1 === breadcrumbs.length ? (
              <span>{breadcrumb}</span>
            ) : (
              <NavLink to={match.url}><>{breadcrumb}</></NavLink>
            )}
          </li>
        ))}
      </ol>
    </div>
  );
};

export default Breadcrumbs;
