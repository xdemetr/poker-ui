import React, { ReactNode } from 'react';
import './PageContainer.scss';

type IProps = {
  size?: 'base' | 'wide';
  mod: string;
  // children: React.ReactNode,

  aside?: ReactNode;
  body?: ReactNode;
} & typeof defaultProps;

const defaultProps = {
  size: 'base',
};

const PageContainer = (props: IProps) => {
  const { mod, size, aside, body } = props;
  const asideClassNames = aside ? 'page-container__aside' : '';
  const bodyClassNames = aside
    ? 'page-container__content page-container__content_with_aside'
    : 'page-container__content';

  return (
    <section className={`page-container page-container_size_${size} ${mod}`}>
      <div className="page-container__body container">
        {aside && <aside className={asideClassNames}>{aside}</aside>}
        {body && <main className={bodyClassNames}>{body}</main>}
      </div>
    </section>
  );
};

PageContainer.defaultProps = defaultProps;

export default PageContainer;
