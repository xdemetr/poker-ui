import moment from 'moment';
import React from 'react';
import { NavLink } from 'react-router-dom';
import { useProfile } from '../../api/profile';
import CONSTANTS from '../../general/constants';
import { ReactComponent as ChartIcon } from './icons/chart.svg';

import { ReactComponent as GamesIcon } from './icons/games.svg';
import { ReactComponent as LoginIcon } from './icons/login.svg';
import { ReactComponent as Logo } from './icons/logo.svg';
import { ReactComponent as LogoutIcon } from './icons/logout.svg';
import { ReactComponent as PlayersIcon } from './icons/players.svg';
import { ReactComponent as SettingsIcon } from './icons/settings.svg';
import { ReactComponent as WineIcon } from './icons/wine.svg';
import './MainHeader.scss';

export const Header: React.FC = () => {
  const { data, logout } = useProfile();
  const currentYear = moment().year();

  const isAuth = data?.user;

  const logoutHandle = async () => {
    logout();
  };


  return (
    <header className="main-header container">
      <span className="main-header__logo">
        <NavLink to={CONSTANTS.ROUTE.HOME}>
          <Logo />
        </NavLink>
      </span>

      <div className="main-header__menu">
        <ul className="main-menu">
          <li className="main-menu__item">
            <NavLink to={CONSTANTS.ROUTE.GAMES} className="main-menu__link">
              <GamesIcon className="main-menu__icon" />
              <span>{CONSTANTS.PAGES.GAMES}</span>
            </NavLink>
          </li>

          <li className="main-menu__item">
            <NavLink to={CONSTANTS.ROUTE.PLAYERS} className="main-menu__link">
              <PlayersIcon className="main-menu__icon" />
              <span>{CONSTANTS.PAGES.PLAYERS}</span>
            </NavLink>
          </li>

          <li className="main-menu__item">
            <NavLink to={CONSTANTS.ROUTE.STAT_BY_YEAR} className="main-menu__link">
              <WineIcon className="main-menu__icon" />
              <span>{currentYear}</span>
            </NavLink>
          </li>

          <li className="main-menu__item">
            <NavLink to={CONSTANTS.ROUTE.CHART} className="main-menu__link">
              <ChartIcon className="main-menu__icon" />
              <span>{CONSTANTS.PAGES.CHART}</span>
            </NavLink>
          </li>

          {isAuth && (
            <li className="main-menu__item">
              <NavLink to={CONSTANTS.ROUTE.PROFILE} className="main-menu__link">
                <SettingsIcon className="main-menu__icon" />
                <span>{CONSTANTS.PAGES.PROFILE}</span>
              </NavLink>
            </li>
          )}

          <li className="main-menu__item">
            {!isAuth && (
              <NavLink to={CONSTANTS.ROUTE.LOGIN} className="main-menu__link">
                <LoginIcon className="main-menu__icon" />
                <span>{CONSTANTS.BUTTON.SIGN_IN}</span>
              </NavLink>
            )}
            {isAuth && (
              <span className="main-menu__link" style={{ cursor: 'pointer' }} onClick={logoutHandle}>
                <LogoutIcon className="main-menu__icon" />
                <span>{CONSTANTS.BUTTON.LOGOUT}</span>
              </span>
            )}
          </li>
        </ul>
      </div>
    </header>
  );
};

export default Header;
