import React from 'react';
import cn from 'classnames';
import './Paginator.scss';

interface Props {
  currentPage: number;
  pages: [{ number: number; url: string }] | null;
  onPageChanged: (p: number) => void;
}

const Paginator: React.FC<Props> = ({ currentPage, onPageChanged, pages }) => {
  if (!pages) {
    return null;
  }

  let pagesRender = [];
  for (let i = 0; i < pages?.length; i++) {
    pagesRender.push(pages[i].number);
  }

  return (
    <ul className="pagination">
      {pagesRender.map((p) => {
        return (
          <li
            key={p}
            className={cn('pagination__item', { pagination__item_active: currentPage === p })}>
            <span onClick={() => onPageChanged(p)} className="pagination__link">
              {p}
            </span>
          </li>
        );
      })}
    </ul>
  );
};

export default Paginator;
