import React from 'react';
import './spinner.css';

const Spinner = () => {
  return (
    <div className="spinner-wrap">
      <div className="lds-grid">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default Spinner;
