import React from 'react';
import { NavLink, useParams } from 'react-router-dom';
import { IPlayer } from '../../general/types/Player';
import { IGame } from '../../general/types/Game';
import Spinner from '../Spinner/Spinner';
import CONSTANTS from '../../general/constants';
import cn from 'classnames';
import './ContentList.scss';

interface Props {
  content: IPlayer[] | IGame[] | null;
  type: string;
  loading: boolean;
  title?: string;
}

const ContentList: React.FC<Props> = ({ content, type, loading, title }) => {
  const { handle = '', name = '' } = useParams<{ handle: string; name: string }>();

  if (loading) {
    return <Spinner />;
  }

  if (!content) {
    return null;
  }

  function link(item: IPlayer | IGame) {
    const isPlayer = type === 'player' ? true : false;
    const linkType = isPlayer ? CONSTANTS.ROUTE.PLAYERS : CONSTANTS.ROUTE.GAMES;
    const linkText = isPlayer ? item?.handle : item.name;

    return `${linkType}/${linkText}`;
  }

  const renderItems = content.map((item) => (
    <li
      key={item.name}
      className={cn('list-group__item', {
        'list-group__item_active': item.name === name || item.handle === handle,
      })}>
      <NavLink to={link(item)} onClick={() => window.scrollTo(0, 0)}>
        {item.name}
      </NavLink>
    </li>
  ));

  return (
    <>
      {title && <h2 className="list-group__title">{title}</h2>}
      <ul className="list-group">{renderItems}</ul>
    </>
  );
};

export default ContentList;
