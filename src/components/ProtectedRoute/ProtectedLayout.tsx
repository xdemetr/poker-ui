import { Navigate, Outlet } from 'react-router-dom';
import { useProfile } from '../../api/profile';
import CONSTANTS from '../../general/constants';

export const ProtectedLayout = () => {
  const { data: isAuth } = useProfile();

  if (!isAuth) {
    return <Navigate to={CONSTANTS.ROUTE.LOGIN} replace />;
  }

  return <Outlet />;
};
