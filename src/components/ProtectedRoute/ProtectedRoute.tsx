import React from 'react';
import { Navigate, Route } from 'react-router-dom';
import { useProfile } from '../../api/profile';
import CONSTANTS from '../../general/constants';

interface ProtectedRouteProps {
  component: React.ComponentType;
  path: string;
}

const ProtectedRoute: React.FC<ProtectedRouteProps> = ({ component: Component, ...rest }) => {
  const { data: isAuth } = useProfile();

  return (
    <Route
      {...rest}
      element={isAuth ? <Component /> : <Navigate to={CONSTANTS.ROUTE.LOGIN} />}
    />
  );
};

export default ProtectedRoute;
