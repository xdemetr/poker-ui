import React, { useEffect, useState } from 'react';
import './Alert.scss';

interface Props {
  type: 'primary' | 'secondary' | 'success' | 'info' | 'warning' | 'danger';
  message: string | null;
}

const defaultProps = {
  delay: 5,
};

const Alert = ({ type, message, delay }: Props & typeof defaultProps) => {
  const [show, setShow] = useState(true);

  useEffect(() => {
    let timer = setTimeout(() => setShow(false), delay * 1000);
    return () => {
      clearTimeout(timer);
    };
  }, [delay]);

  if (!message) {
    return null;
  }
  return show ? <div className={`alert alert_${type}`}>{message}</div> : null;
};

export default Alert;

Alert.defaultProps = defaultProps;
