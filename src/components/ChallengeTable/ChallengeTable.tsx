import React from 'react';
import { IPlayer } from '../../general/types/Player';
import ChallengeItem from './ChallengeItem';

export interface IPlayerWithTotal extends Partial<IPlayer> {
  totalResult: number;
}

interface ChallengeTableProps {
  players: IPlayerWithTotal[];
}

const ChallengeTable = ({ players }: ChallengeTableProps) => {
  return (
    <div className="challenge-table">
      {players.map((player, index) => <ChallengeItem player={player} index={index} key={`challenge-${index}`} />)}
    </div>
  );
};

export default ChallengeTable;
