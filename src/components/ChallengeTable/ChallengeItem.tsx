import cn from 'classnames';
import React, { useState } from 'react';
import { IPlayerWithTotal } from './ChallengeTable';
import { ReactComponent as PlusIcon } from '../../assets/images/plus.svg';
import { ReactComponent as MinusIcon } from '../..//assets/images/minus.svg';

interface ChallengeItemProps {
  player: IPlayerWithTotal;
  index: number;
}

const ChallengeItem = ({ player, index }: ChallengeItemProps) => {
  const [collapsed, setCollapsed] = useState(true);

  const handleToggle = () => {
    setCollapsed(!collapsed);
  };

  if (!player.results) {
    return null;
  }

  return (
    <section className={cn('player-results', { 'player-results_collapsed': collapsed })}>
      <header className="player-results__header" onClick={handleToggle}>
          <span className="player-results__header-icon">
            {collapsed ? <PlusIcon /> : <MinusIcon />}
          </span>
        <span className="player-results__header-title">
          <span className="badge badge_info" style={{ margin: '0 8px 0 0' }}>{index + 1}</span>
          {player.name}</span>
        <span
          className={cn(
            'badge ms-auto player-results__header-result',
            { badge_danger: player.totalResult < 0 },
            { badge_success: player.totalResult > 0 },
          )}>
                {player.totalResult}
          {/*{result.result}*/}
          </span>
      </header>

      <div className="player-results__body">

        <table className={cn('table')}>
          <tbody>
          {
            player.results.map((result, index) => (
              <tr key={`tr-rating-${index}`}
                  className={cn({ table_danger: result.result < 0 }, { table_success: result.result > 0 })}>
                <td>
                  {result.game.name}
                </td>
                <td>
                  {result.result}
                </td>
              </tr>
            ))
          }
          </tbody>
        </table>
      </div>
    </section>
  );
};

export default ChallengeItem;
