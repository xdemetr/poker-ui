import React from 'react';
import { useProfile } from '../../api/profile';

interface Props {
  children: React.ReactElement;
  classNames?: string;
}

const ProtectedElement: React.FC<Props> = ({ children, classNames }) => {
  const { data: isAuth, isExp, logout } = useProfile();

  if (isAuth && isExp) {
    logout();
  }

  if (!isAuth) {
    return null;
  }

  return <div className={classNames}>{children}</div>;
};

export default ProtectedElement;
