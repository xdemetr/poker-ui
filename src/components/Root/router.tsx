import { createBrowserRouter } from 'react-router-dom';
import App from '../../App';
import CONSTANTS from '../../general/constants';
import {
  AuthPage,
  ChallengePage,
  ChartPage,
  EditGamePage,
  EditPlayerPage,
  GamePage,
  HomePage,
  NewGamePage,
  PlayersPage,
  ProfilePage,
} from '../../pages/pages';
import { ProtectedLayout } from '../ProtectedRoute/ProtectedLayout';
import Root from './Root';

export const router = createBrowserRouter([
  {
    element: <App />,
    children: [
      {
        path: '/',
        element: <Root />,
        children: [
          {
            path: '/',
            element: <HomePage />,
          },
          {
            path: CONSTANTS.ROUTE.GAMES,
            element: <GamePage />,
          },
          {
            path: CONSTANTS.ROUTE.GAMES + '/:name?',
            element: <GamePage />,
          },
          {
            path: CONSTANTS.ROUTE.PLAYERS,
            element: <PlayersPage />,
          },
          {
            path: CONSTANTS.ROUTE.PLAYERS + '/:handle?',
            element: <PlayersPage />,
          },
          {
            path: CONSTANTS.ROUTE.STAT_BY_YEAR,
            element: <ChallengePage />,
          },
          {
            path: CONSTANTS.ROUTE.CHART,
            element: <ChartPage />,
          },
          {
            path: CONSTANTS.ROUTE.LOGIN,
            element: <AuthPage />,
          },
          {
            element: <ProtectedLayout />,
            children: [
              {
                path: CONSTANTS.ROUTE.PROFILE,
                element: <ProfilePage />,
              },
              {
                path: CONSTANTS.ROUTE.PLAYERS_NEW,
                element: <EditPlayerPage />,
              },
              {
                path: CONSTANTS.ROUTE.PLAYERS + '/:handle?/edit',
                element: <EditPlayerPage />,
              },
              {
                path: CONSTANTS.ROUTE.GAME_NEW,
                element: <NewGamePage />,
              },
              {
                path: CONSTANTS.ROUTE.GAMES + '/:name?/edit',
                element: <EditGamePage />,
              },
              {
                path: CONSTANTS.ROUTE.PROFILE,
                element: <ProfilePage />,
              },
            ],
          },
        ],
      },
    ],
  },
]);
